resource "aws_ecr_repository" "api" {
  name                 = lower("${var.company}-${var.project}-${var.environment}-ecr-${var.service}")
 
  image_scanning_configuration {
    scan_on_push = true
  }
}
