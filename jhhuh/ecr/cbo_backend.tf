terraform {
  backend "s3" {
    bucket = "terraform-state-053934082118-ap-northeast-2"
    key    = "cbo-dev/cbo-ecr.tfstate"
    region = "ap-northeast-2"
    dynamodb_table = "terraform-state-053934082118-ap-northeast-2"
    encrypt       = true
  }
}