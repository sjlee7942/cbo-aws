variable "aws_region"{
    type = string
}

variable "s3_bucket" {
  type = string
}

variable "s3_folder_project" {
  type = string
}

variable "s3_bucket_region" {
  type = string
}

variable "s3_tfstate_file" {
  type = string
}

# ecr
variable "company" {
    type = string
}
variable "project" {
    type = string
}
variable "environment" {
    type = string
}
variable "service" {
    type = string
}

