aws_region = "ap-northeast-2"

########## Terraform State file configuration
s3_bucket = "terraform-state-053934082118-ap-northeast-2"
s3_folder_project = "cbo-dev"
s3_bucket_region = "ap-northeast-2"
s3_tfstate_file="cbo-ecr.tfstate"

# ecr
company = "SDS"
project = "CBO"
environment = "DEV"
service = "STUDY"
